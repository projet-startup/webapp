
> Février 2021

# FrontEnd du projet StartUp POC (FISE3)
Projet du group A1 : **_Corentin Ferlay, Julien Giovianzzo, Malo Saunier, et Kenza Yahiaoui_**

## Technologies Front
Le site est concue en **AngluarJs 8**.
Le site est responsive et est utilisable pour tous les utilisateurs sur mobile, tablette et ordinateurs.
Pour compiler le front : `npm start`. Il utilise le port 8080 par défault.

## Projet BackEnd
Vous trouverez le repository de notre backend ici: https://gitlab.com/projet-startup/server 

## MockAPI
Pour nous aider à développer le front et le back en simultané, nous avons mis en place un Mock API simpliste.
Technologie utilisée :JSON Server https://medium.com/codingthesmartway-com-blog/create-a-rest-api-with-json-server-36da8680136d 

Pour lancer le mock server : `npm run api`.
Il faut changer la variable dans globals.ts pour que les appels d'API aille a notre serveur local.
