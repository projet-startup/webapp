// login handling
module.exports = (req, res, next) => {
    if (req.path == '/login') {
        if (req.body.email === 'dev') {
            console.log("DEV login.");
            res.status(200).json(
                {
                    "user": {
                        "managerId": "820f8f3e-a2ca-4881-aecc-66db4fac65b6",
                        "id": "a092dd4f-f2eb-4495-9952-6bdb660defbc",
                        "firstname": "Maol",
                        "lastname": "Sonié",
                        "email": "test@dev.com",
                        "role": "DEV",
                        "projects": [
                            {
                                "id": "912fe140-26cd-4a5e-aadb-ce682e0a248f",
                                "name": "projet test"
                            }]
                    },
                    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InJvbGUiOiJERVYiLCJpZCI6ImEwOTJkZDRmLWYyZWItNDQ5NS05OTUyLTZiZGI2NjBkZWZiYyJ9LCJkYXRlIjoxNjEwNTU4MzY2NDc3LCJpYXQiOjE2MTA1NTgzNjYsImV4cCI6MTYxMDU2MTk2Nn0.1B5wzKICeDaNVdOUg_dpbz8Xar_f4dwDUJATqt2zM2U"
                });

        } else if (req.body.email === 'manager') {
            console.log("MANAGER login.");
            res.status(200).json(
                {
                    "user": {
                        "managerId":"",
                        "id":"820f8f3e-a2ca-4881-aecc-66db4fac65b6",
                        "firstname":"Papa",
                        "lastname":"Sonié",
                        "email": "test@manager.com",
                        "role":"MANAGER",
                        "projects":[]
                    },
                    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InJvbGUiOiJNQU5BR0VSIiwiaWQiOiI4MjBmOGYzZS1hMmNhLTQ4ODEtYWVjYy02NmRiNGZhYzY1YjYifSwiZGF0ZSI6MTYxMDU1ODUzNTMzNCwiaWF0IjoxNjEwNTU4NTM1LCJleHAiOjE2MTA1NjIxMzV9.C1KOl3UO9_6jDw_LuY5WxS_U6-yLgAwa6vdZZHBYrUU"
                });
        } else if (req.body.email === 'admin') {
            console.log("ADMIN login.");
            res.status(200).json(
                {
                    "user": {
                        "managerId":"",
                        "id":"23e1bd02-33ba-4ab0-972e-cd21ac4e70c9",
                        "firstname":"admin",
                        "lastname":"Test",
                        "email": "test@admin.com",
                        "role":"ADMIN",
                        "projects":[]
                    },
                    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InJvbGUiOiJBRE1JTiIsImlkIjoiMjNlMWJkMDItMzNiYS00YWIwLTk3MmUtY2QyMWFjNGU3MGM5In0sImRhdGUiOjE2MTA1NTg0ODIwMDgsImlhdCI6MTYxMDU1ODQ4MiwiZXhwIjoxNjEwNTYyMDgyfQ.EAz_JOr3Drshe0koDkkt5UBBStvHsebKAyPY4h6AU4E"
                });
        } else {
            console.log("email unknown.");
            res.end();
        }
    } else {
        next()
    }
}