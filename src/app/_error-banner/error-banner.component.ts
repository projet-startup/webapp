import { Component, Input, OnInit } from "@angular/core";

@Component({ selector: "error-banner", templateUrl: 'error-banner.component.html' })
export class ErrorBannerComponent implements OnInit {
    @Input()
    msg : string;

    constructor() { }

    ngOnInit() { }
}