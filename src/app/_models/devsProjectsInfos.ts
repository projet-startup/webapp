import { UsertimeSelectedUser } from "./usertimeSelectedUser";

export class DevsProjectsInfos {
    id: string;
    firstname: string;
    lastname: string;
    email: string;
    projects: Array<UsertimeSelectedUser>;

    constructor(devId: string, firstname: string, lastname: string, email: string, projects?: Array<UsertimeSelectedUser>) {
        this.id = devId;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.projects = projects;
      }
}