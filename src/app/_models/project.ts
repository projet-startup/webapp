import { User } from "./user";

export class Project {
    id: string;
    name: string;
    description: string;
    isOpen: boolean;
    devs: Array<any>;

    constructor(id: string, name: string, description: string, isOpen: boolean, devs?: Array<any>){
        this.id = id;
        this.name = name;
        this.description = description;
        this.isOpen = isOpen;
        this.devs = devs;
    }
}