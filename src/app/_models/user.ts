﻿export class User {
    id: string;
    firstname: string;
    lastname: string;
    email: string;
    role: string;
    managerId: string;
    projects: Array<any>;
    token: string;

    constructor(id: string, firstname: string, lastname: string){
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
    }
}