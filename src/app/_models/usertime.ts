export class Usertime {
    id: string;
    projectId: string;
    project: any;  // {id:"", name:""}
    time: number;
    date: Date;
    isLocked: boolean;
}