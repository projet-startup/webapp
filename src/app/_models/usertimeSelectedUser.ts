export class UsertimeSelectedUser {
    projectId: string;
    projectName: string;
    totalTime: number;
    curMonth: number;
    oldest_ut_date : Date;

    constructor(projectId?: string, projectName?: string, totalTime?: number, curMonth?: number, oldest_ut_date? : Date) {
        this.projectId = projectId;
        this.projectName = projectName;
        this.totalTime = totalTime;
        this.curMonth = curMonth;
        this.oldest_ut_date = oldest_ut_date;
      }
}