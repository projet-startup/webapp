﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '@/_models';
import { GlobalConstants } from "../../globals";

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(
        private http: HttpClient,
    ) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    test_test() {
        this.http.get<any>(GlobalConstants.apiURL + 'users').subscribe((val) => {
            console.log(val);
        });
    }

    login(email: string, password: string) {
        console.warn(GlobalConstants.apiURL +  'login');
        return this.http.post<any>(GlobalConstants.apiURL + 'login', { email, password })
            .pipe(map(res => {
                console.log("res", res);
                /* {  // Exemple du format de res
                    token: "un-token",
                    user: {...}
                }*/

                // Create the User object to store
                let user: User = res.user;
                user.token = res.token;
                console.log(JSON.stringify(user));

                // Store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(user));
                this.currentUserSubject.next(user);
                return res;
            }));
    }

    logout() {
        // remove user from local storage and set current user to null
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}