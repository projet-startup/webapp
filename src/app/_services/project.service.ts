import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Project} from '@/_models';
import { GlobalConstants } from "../../globals";
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ProjectService {
    constructor(private http: HttpClient) { }

    getAll() {
        console.warn("GET: " + GlobalConstants.apiURL + "projects");
        return this.http.get(GlobalConstants.apiURL + 'projects');
    }

    getDevsFromProject(projectId: string): Observable<any> {
        console.warn("GET: " + GlobalConstants.apiURL + "projects/" + projectId + "/developers")
        return this.http.get<any>(GlobalConstants.apiURL + "projects/" + projectId + "/developers");
    }

    create(projectName: string, projectDescription: string): Observable<any> {
        let body = {
            "name": projectName,
            "description": projectDescription
        };
        console.warn("POST: " + GlobalConstants.apiURL + "projects", body);
        return this.http.post<any>(GlobalConstants.apiURL + "projects", body);
    }

    updateProject(projectId: string, projectName: string, projectDescription: string): Observable<any> {
        let body = {
            "name": projectName,
            "description": projectDescription
        };
        console.warn("PATCH :" + GlobalConstants.apiURL + "projects/" + projectId, body);
        return this.http.patch<any>(GlobalConstants.apiURL + "projects/" + projectId, body);
    }

    delete(projectId: string): Observable<any> {
        console.warn("DELETE : " + GlobalConstants.apiURL + "projects/" + projectId)
        return this.http.delete(GlobalConstants.apiURL + "projects/" + projectId);
    }

}