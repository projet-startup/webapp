﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '@/_models';
import { GlobalConstants } from "../../globals";
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        console.warn("GET: " + GlobalConstants.apiURL + "users");
        return this.http.get(GlobalConstants.apiURL + 'users');
    }

    delete(id: string): Observable<any> {
        console.warn("DEELTE: " + GlobalConstants.apiURL + "users/" + id);
        return this.http.delete(GlobalConstants.apiURL + 'users/' + id);
    }

    getUserData(id: string): Observable<User> {
        console.warn("GET: " + GlobalConstants.apiURL + "users/" + id);
        return this.http.get<User>(GlobalConstants.apiURL + "users/" + id);
    }


    deleteUserFromProject(userId: string, projectId: string) {
        console.warn("DELETE: " + GlobalConstants.apiURL + "users/" + userId + "/project/" + projectId);
        return this.http.delete(GlobalConstants.apiURL + "users/" + userId + "/project/" + projectId);
    }

    create(nom: string, prenom: string, email: string, pass: string, role: string) {
        const body = {
            "firstname": prenom,
            "lastname": nom,
            "email": email,
            "password": pass,
            "role": role
        };
        console.warn("POST: " + GlobalConstants.apiURL + "users  body:" + JSON.stringify(body));
        return this.http.post(GlobalConstants.apiURL + "users", body);
    }

    updateData(uid: string, nom: string, prenom: string, email: string) {
        const body = {
            "firstname": prenom,
            "lastname": nom,
            "email": email,
        };
        console.warn("PATCH: " + GlobalConstants.apiURL + "users/" + uid + "  body:" + JSON.stringify(body));
        return this.http.patch(GlobalConstants.apiURL + "users/" + uid, body);
    }

    updatePassword(uid: string, newpass: string) {
        const body = {
            "newPassword": newpass
        };
        console.warn("PATCH: " + GlobalConstants.apiURL + "users/" + uid + "/password  body:" + JSON.stringify(body));
        return this.http.patch(GlobalConstants.apiURL + "users/" + uid + "/password", body);
    }

    updateRole(uid: string, role: string) {
        const body = {
            "role": role
        };
        console.warn("PATCH: " + GlobalConstants.apiURL + "users/" + uid + "/role  body:" + JSON.stringify(body));
        return this.http.patch(GlobalConstants.apiURL + "users/" + uid + "/role", body);
    }
    
    updateManager(uid: string, new_mid: string) {
        const body = {
            "managerId": new_mid
        };
        console.warn("PATCH: " + GlobalConstants.apiURL + "users/" + uid + "/manager  body:" + JSON.stringify(body));
        return this.http.patch(GlobalConstants.apiURL + "users/" + uid + "/manager", body);
    }
    
    addUserToProject(userId: string, projectId: string): Observable<any> {
        let body = {
            "projectId": projectId
        };
        console.warn("PATCH: " + GlobalConstants.apiURL + "users/" + userId + "/project", body);
        return this.http.patch<any>(GlobalConstants.apiURL + "users/" + userId + "/project", body);
    }

}