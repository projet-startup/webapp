import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { GlobalConstants } from "../../globals";
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UsertimeService {
    constructor(private http: HttpClient) { }

    delete(utId: string): Observable<any> {
        console.warn("DELETE: " + GlobalConstants.apiURL + "usertimes/" + utId);
        return this.http.delete(GlobalConstants.apiURL + "usertimes/" + utId);
    }

    getUsertimesCR(userId: string, curMonth: boolean): Observable<any> {
        return this.http.get<any>(GlobalConstants.apiURL + "usertimes/user/" + userId + "?curMonth=" + curMonth);
    }

    getUsertimes(userId: string, chrono: boolean=false): Observable<any> {
        console.warn("GET: " + GlobalConstants.apiURL + "usertimes/user/" + userId + "?chrono=" + chrono);
        return this.http.get<any>(GlobalConstants.apiURL + "usertimes/user/" + userId + "?chrono=" + chrono);
    }

    getUsertimesActiveProjects(userId: string, active: boolean): Observable<any> {
        console.warn("GET : " + GlobalConstants.apiURL + "usertimes/user/" + userId + "?active=" + active);
        return this.http.get<any>(GlobalConstants.apiURL + "usertimes/user/" + userId + "?active=" + active);
    }

    create(projectId: string, date: Date, time: number): Observable<any> {
        let body = {
            "projectId": projectId,
            "time": time,
            "date": date
        };
        console.warn("POST: " + GlobalConstants.apiURL + "usertimes ", body);
        return this.http.post<any>(GlobalConstants.apiURL + "usertimes", body);
    }

    getPDF(userId: string, date_str: string): Observable<any> {
        // date_str format : YYYY-MM-DD
        console.warn("GET: " + GlobalConstants.apiURL + "usertimes/" + userId + "/PDF/" + date_str);
        return this.http.get(GlobalConstants.apiURL + "usertimes/" + userId + "/PDF/" + date_str, {responseType: 'blob'});
    }

    updateUsertime(utId: string, projectId: string, date_str:string, time: number): Observable<any> {
        let body = {
            "projectId": projectId,
            "time": time,
            "date": date_str
        };
        console.warn("PATCH: " + GlobalConstants.apiURL + "usertimes/" + utId, body);
        return this.http.patch<any>(GlobalConstants.apiURL + "usertimes/" + utId, body);
    }
}