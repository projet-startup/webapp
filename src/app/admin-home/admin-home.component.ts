﻿import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';

import { Project, User } from '@/_models';
import { UserService, AuthenticationService, ProjectService } from '@/_services';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ModalComponent } from 'angular-custom-modal';


@Component({ templateUrl: 'admin-home.component.html' })
export class AdminHomeComponent implements OnInit, OnDestroy {
    currentUser: User;
    createForm: FormGroup;
    modForm:    FormGroup;
    roleForm:   FormGroup;
    passForm:   FormGroup;
    filterForm: FormGroup;
    users : Array<User> = [];
    managers : Array<User> = [];
    projects : Array<Project> = [];
    errMsg : string = "";
    loading: boolean;
    selected_user: User = null;
    createUserLoading: boolean = false;
    deleteUserLoading: boolean = false;
    modifyUserLoading: boolean = false;
    setRoleLoading:    number  = 0;  // 0 == false. >0 == true
    modPassLoading:    boolean = false;

    getAllUsers$: Subscription;
    getAllProjects$: Subscription;
    createUser$: Subscription;
    deleteUser$: Subscription;
    updateUser$: Subscription;
    updateRole$: Subscription;
    updateManager$: Subscription;
    updatePass$: Subscription;

    // convenience getter for easy access to form fields
    get f()  { return this.createForm.controls; }
    get fm() { return this.modForm.controls;    }
    get fr() { return this.roleForm.controls;   }
    get fp() { return this.passForm.controls;   }
    get ff() { return this.filterForm.controls; }

    @ViewChild('newUserModal', {static: false}) newUserModal:ModalComponent;
    @ViewChild('delUserModal', {static: false}) delUserModal:ModalComponent;
    @ViewChild('modUserModal', {static: false}) modUserModal:ModalComponent;
    @ViewChild('setRoleModal', {static: false}) setRoleModal:ModalComponent;
    @ViewChild('modPassModal', {static: false}) modPassModal:ModalComponent;

    constructor(
        private authenticationService: AuthenticationService,
        private formBuilder: FormBuilder,
        private userService: UserService,
        private projectSerivce: ProjectService,
    ) {
        this.currentUser = this.authenticationService.currentUserValue;
    }

    ngOnInit() {
        this.createForm = this.formBuilder.group({
            nom: "",
            prenom: "",
            email: "",
            pass: "",
            role: "DEV"
        });
        this.modForm = this.formBuilder.group({
            id: "",
            nom: "",
            prenom: "",
            email: "",
        });
        this.roleForm = this.formBuilder.group({
            manager: "",
            role: "",
        });
        this.passForm = this.formBuilder.group({
            uid: "",
            pass: "",
        });
        this.filterForm = this.formBuilder.group({
            role: "",
            nom: "",
            nom_m: "",  // Manager name
            projet: "",
        });
        this.onRefreshBtn();
    }

    ngOnDestroy() {
        if (this.getAllUsers$)    this.getAllUsers$.unsubscribe();
        if (this.getAllProjects$) this.getAllProjects$.unsubscribe();
        if (this.createUser$)     this.createUser$.unsubscribe();
        if (this.deleteUser$)     this.deleteUser$.unsubscribe();
        if (this.updateUser$)     this.updateUser$.unsubscribe();
        if (this.updateRole$)     this.updateRole$.unsubscribe();
        if (this.updateManager$)  this.updateManager$.unsubscribe();
        if (this.updatePass$)     this.updatePass$.unsubscribe();
    }

    displayError(msg) {
        this.errMsg = msg;
    }

    onRefreshBtn() {
        this.displayError("");
        this.refreshList();
    }

    refreshList() {
        this.loading = true;
        this.projects = [];
        this.users = [];
        this.getAllProjects$ = this.projectSerivce.getAll().subscribe(
            (res) => {
                this.projects = res['projects'];
            },
            (err) => {
                this.displayError("Impossible de recuperer les projets (" + err + ")");
            }
        )
        this.getAllUsers$ = this.userService.getAll().subscribe(
            (res) => {
                this.users = res['users'];
                this.managers = this.users.filter(u => u.role == "MANAGER");
                this.applyFilters();
                this.loading = false;
            },
            (err) => {
                this.displayError("Impossible de recuperer les utilisateurs (" + err + ")");
            }
        );
    }

    applyFilters() {
        if (this.ff.role.value != "") {
            this.users = this.users.filter(u => u.role == this.ff.role.value);
        }

        if (this.ff.nom.value != "") {
            this.users = this.users.filter(u => (u.firstname + ' ' + u.lastname).toLowerCase().includes(this.ff.nom.value.toLowerCase()) 
                || (u.lastname + ' ' + u.firstname).toLowerCase().includes(this.ff.nom.value.toLowerCase()));
        }

        if (this.ff.nom_m.value != "") {
            const managerIDs_filtered: Array<string> = this.managers
                .filter(m => 
                       (m.firstname + ' ' + m.lastname).toLowerCase().includes(this.ff.nom_m.value.toLowerCase()) 
                    || (m.lastname + ' ' + m.firstname).toLowerCase().includes(this.ff.nom_m.value.toLowerCase()))
                .map(m => m.id);
            this.users = this.users.filter(u =>  managerIDs_filtered.includes(u.managerId));
        }
        if (this.ff.projet.value != "") {
            this.users = this.users.filter(u => u.projects.filter(p => p.id == this.ff.projet.value).length > 0);
        }
    }

    onRoleFilterChange(val: string) {
        if (val != '' && val != "DEV") {
            // Disable manager name and projet filters
            this.ff.nom_m.setValue("");
            this.ff.projet.setValue("");
            this.ff.nom_m.disable();
            this.ff.projet.disable();
        } else {
            this.ff.nom_m.enable();
            this.ff.projet.enable();
        }
    }

    onCreateUser() {
        this.displayError("");
        this.createUserLoading = true;
        this.createUser$ = this.userService.create(this.f.nom.value, this.f.prenom.value, this.f.email.value, this.f.pass.value, this.f.role.value).subscribe(
            () => {
                this.f.nom.setValue("");
                this.f.prenom.setValue("");
                this.f.email.setValue("");
                this.f.pass.setValue("");
                this.f.role.setValue("DEV");
                this.newUserModal.close();
                this.createUserLoading = false;
                this.refreshList();
            },
            (err) => {
                this.displayError("Impossible de créer l'utilisateur (" + err + ")");
                this.f.nom.setValue("");
                this.f.prenom.setValue("");
                this.f.email.setValue("");
                this.f.pass.setValue("");
                this.f.role.setValue("DEV");
                this.createUserLoading = false;
                this.newUserModal.close();
            }
        );
    }

    onDeleteUser(uid: string) {
        this.displayError("");
        this.deleteUserLoading = true;
        this.deleteUser$ = this.userService.delete(uid).subscribe(
            () => {
                this.refreshList();
                this.delUserModal.close();
                this.deleteUserLoading = false;
            },
            (err) => {
                this.displayError("Impossible de supprimer l'utilisateur (" + err + ")");
                this.delUserModal.close();
                this.deleteUserLoading = false;
            }
        );
    }

    onPencilClick(user: User) {
        this.modUserModal.open();
        this.fm.nom.setValue(user.lastname);
        this.fm.prenom.setValue(user.firstname);
        this.fm.email.setValue(user.email);
        this.fm.id.setValue(user.id);
    }

    onModifyUser() {
        this.displayError("");
        this.modifyUserLoading = true;
        this.deleteUser$ = this.userService.updateData(this.fm.id.value, this.fm.nom.value, this.fm.prenom.value, this.fm.email.value).subscribe(
            () => {
                this.refreshList();
                this.modUserModal.close();
                this.modifyUserLoading = false;
            },
            (err) => {
                this.displayError("Impossible de supprimer l'utilisateur (" + err + ")");
                this.modUserModal.close();
                this.modifyUserLoading = false;
            }
        );
    }

    onKeyClick(user: User) {
        this.selected_user = user;
        this.fp.uid.setValue(user.id);
        this.fp.pass.setValue("");
        this.modPassModal.open();
    }

    onCardClick(user: User) {
        this.selected_user = user;
        this.fr.role.setValue(user.role);
        if (user.role == "DEV") {
            if (user.managerId)
                this.fr.manager.setValue(user.managerId);
            else
                this.fr.manager.setValue("");
        }
        this.setRoleModal.open();
    }

    onPlusClick(i: number) {
        // Faire apparaitre les boutons d'édition
        for (let j = 0; j < this.users.length; j++) {
            document.getElementById("plus" + j.toString()).style.display = "inline-block";
            document.getElementById("edit-buttons" + j.toString()).style.display = "none";
        }
        document.getElementById("plus" + i.toString()).style.display = "none";
        document.getElementById("edit-buttons" + i.toString()).style.display = "inline-block";
    }

    onMinusClick(i: number) {
        // Faire disparaitre les boutons d'édition
        document.getElementById("plus" + i.toString()).style.display = "inline-block";
        document.getElementById("edit-buttons" + i.toString()).style.display = "none";
    }


    onRoleChange(uid: string) {
        this.displayError("");

        
        this.setRoleLoading += 1;
        this.updateRole$ = this.userService.updateRole(uid, this.fr.role.value).subscribe(
            () => {
                this.setRoleLoading -= 1;
                if (this.setRoleLoading == 0) {
                    this.refreshList();
                    this.setRoleModal.close();
                }
                
                if (this.fr.role.value == 'DEV') {
                    this.setRoleLoading += 1;
                    this.updateManager$ = this.userService.updateManager(uid, this.fr.manager.value).subscribe(
                        () => {
                            this.setRoleLoading -= 1;
                            if (this.setRoleLoading == 0) {
                                this.refreshList();
                                this.setRoleModal.close();
                            } 
                        },
                        (err) => {
                            this.displayError("Impossible de modifier le manager (" + err + ")");
                            this.setRoleLoading -= 1;
                            if (this.setRoleLoading == 0) {
                                this.setRoleModal.close();
                            } 
                        }
                    );
                }
            },
            (err) => {
                this.displayError("Impossible de modifier le role (" + err + ")");
                this.setRoleLoading -= 1;
                if (this.setRoleLoading == 0) {
                    this.setRoleModal.close();
                } 
            }
        );
        
    }

    onChangePass(uid: string) {
        this.displayError("");
        this.modPassLoading = true;
        this.updatePass$ = this.userService.updatePassword(uid, this.fp.pass.value).subscribe(
            () => {
                this.modPassLoading = false;
                this.modPassModal.close();
                this.refreshList();
            },
            (err) => {
                this.displayError("Impossible de modifier le mot de passe (" + err + ").");
                this.modPassLoading = false;
                this.modPassModal.close();
            }
        );
    }
}