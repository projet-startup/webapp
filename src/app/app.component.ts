﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from './_services';
import { User } from './_models';

import './_content/app.less';
import './_content/animations.less';

@Component({ selector: 'app', templateUrl: 'app.component.html' })
export class AppComponent implements OnInit {
    currentUser: User;
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
        this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    }

    ngOnInit() {
        // Sans ça, impossible de refresh une page (meme en changeant les query params)
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    }

    logout() {
        this.authenticationService.logout();
        this.router.navigate(['/login']);
    }
}