﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { appRoutingModule } from './app.routing';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { AppComponent } from './app.component';

import { ErrorBannerComponent } from './_error-banner'
import { DevHomeComponent } from './dev-home';
import { DevCreateComponent } from './dev-create';
import { DevExportComponent } from './dev-export';
import { ManagerHomeComponent } from './manager-home';
import { AdminHomeComponent } from './admin-home';
import { LoginComponent } from './login';
import { AlertComponent } from './_components';
import { ModalModule } from 'angular-custom-modal';
import { ManagerProjectViewComponent } from './manager-project-view';
import { ProjectCreateComponent } from './project-create';

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        appRoutingModule,
        FormsModule,
        ModalModule
    ],
    declarations: [
        AppComponent,
        DevHomeComponent,
        DevCreateComponent,
        DevExportComponent,
        ManagerHomeComponent,
        AdminHomeComponent,
        LoginComponent,
        AlertComponent,
        ErrorBannerComponent,
        ManagerProjectViewComponent,
        ProjectCreateComponent
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { };