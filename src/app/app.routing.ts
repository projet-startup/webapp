﻿import { Routes, RouterModule } from '@angular/router';

import { DevHomeComponent } from './dev-home';
import { ManagerHomeComponent } from './manager-home';
import { AdminHomeComponent } from './admin-home';
import { LoginComponent } from './login';
import { AuthGuard } from './_helpers';
import { ManagerProjectViewComponent } from './manager-project-view';

const routes: Routes = [
    { path: 'dev-home', component: DevHomeComponent, canActivate: [AuthGuard] },
    { path: 'manager-home', component: ManagerHomeComponent, canActivate: [AuthGuard] },
    { path: 'admin-home', component: AdminHomeComponent, canActivate: [AuthGuard] },
    { path: 'manager-project-view', component: ManagerProjectViewComponent, canActivate: [AuthGuard]},
    { path: 'login', component: LoginComponent },
    { path: '', redirectTo: '/login', pathMatch: 'full'},  // '/' => '/login'

    // otherwise redirect to home
    { path: '**', redirectTo: '/login?not_found_error=true' }
];

export const appRoutingModule = RouterModule.forRoot(routes); 