﻿import { Component, OnDestroy, OnInit, Output } from '@angular/core';

import { User } from '@/_models';
import { UsertimeService, AuthenticationService } from '@/_services';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';


@Component({selector:"dev-create", templateUrl: 'dev-create.component.html' })
export class DevCreateComponent implements OnInit, OnDestroy {
    currentUser: User;
    createForm: FormGroup;
    projects: Array<any>;
    shakeDate: boolean = false;

    userCreate$ : Subscription;

    @Output() refreshUsertimes : EventEmitter<null> = new EventEmitter<null>();
    @Output() displayError : EventEmitter<string> = new EventEmitter<string>();

    // convenience getter for easy access to form fields
    get f() { return this.createForm.controls; }


    constructor(
        private authenticationService: AuthenticationService,
        private usertimeService: UsertimeService,
        private formBuilder: FormBuilder,
    ) {
        this.currentUser = this.authenticationService.currentUserValue;
        this.projects = [];
        for (const p of this.currentUser.projects) {
            this.projects.push({value: p.id, name: p.name});
        }
    }

    ngOnInit() {
        this.createForm = this.formBuilder.group({
            projet: '',
            date: '',
            temps: ''
        });
    }

    ngOnDestroy() {
        if (this.userCreate$) this.userCreate$.unsubscribe();
    }

    onSubmit(data) {
        this.userCreate$ = this.usertimeService.create(data.projet, data.date, data.temps)
        .subscribe(() => {  // Successful response from back end
            this.refreshUsertimes.emit(null);
        },
        (err) => {  // Error response from backend (after passing through ErrorInterceptor)
            console.error("Erreur de création de pointage : ", err);
            this.displayError.emit("Erreur de création de pointage (" + err + ")");
        });
    }

    onBtnTodayClick() {
        let d : Date = new Date();  // Today
        let year: string = d.getFullYear().toString();
        let month: string = (d.getMonth() + 1).toString().padStart(2, '0');
        let day: string = d.getDate().toString().padStart(2, '0');
        // console.log(year + '-' + month + '-' + day);

        this.f.date.setValue(year + '-' + month + '-' + day);
        this.shakeDate = true;
        this.animationTimer();
    }

    animationTimer() {
        let self = this;
        setTimeout(function() {self.shakeDate = false;}, 1000);
    }

    onBtnTimeClick(t) {
        this.f.temps.setValue(t.toString());
    }
}