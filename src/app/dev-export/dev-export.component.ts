﻿import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChange, SimpleChanges } from '@angular/core';

import { User } from '@/_models';
import { AuthenticationService, UsertimeService } from '@/_services';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { saveAs } from 'file-saver';

@Component({ selector: "dev-export", templateUrl: 'dev-export.component.html' })
export class DevExportComponent implements OnInit, OnDestroy, OnChanges {
    currentUser: User;
    createForm: FormGroup;
    mois: Array<any>;
    annees: Array<any>;

    getPDF$ : Subscription;
    
    @Input() oldest_ut_date : string;
    @Input() dev : any;
    @Output() displayError : EventEmitter<string> = new EventEmitter<string>();

    // convenience getter for easy access to form fields
    get f() { return this.createForm.controls; }


    constructor(
        private authenticationService: AuthenticationService,
        private formBuilder: FormBuilder,
        private usertimeService: UsertimeService,
    ) {
        this.currentUser = this.authenticationService.currentUserValue;
        this.mois = [
            "Janvier",
            "Fevrier",
            "Mars",
            "Avril",
            "Mai",
            "Juin",
            "Juillet",
            "Août",
            "Septembre",
            "Octobre",
            "Novembre",
            "Decembre",
        ];
        
        this.annees = [];
    }

    ngOnInit() {
        // Selectionner le mois précedent par défaut
        let d = new Date();
        if (d.getMonth() == 0) {  // Si janvier
            d.setMonth(11);
            d.setFullYear(d.getFullYear() - 1);
        } else {  // Si après Janvier
            d.setMonth(d.getMonth() - 1);
        }

        this.createForm = this.formBuilder.group({
            mois: d.getMonth().toString(),
            annee: d.getFullYear().toString(),
        });
    }

    ngOnDestroy() {
        if (this.getPDF$) this.getPDF$.unsubscribe();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.oldest_ut_date) {
            let d = new Date();
            this.annees = [];
            for (let i = d.getFullYear(); i >= new Date(this.oldest_ut_date).getFullYear(); i--) {
                this.annees.push(i);
            }
        }
    }

    onSubmit(data) {
        // data = {mois: "[0-11]", annee: "XXXX"}
        const year: string = data.annee;
        const month: string = (parseInt(data.mois) + 1).toString().padStart(2, '0');
        const day: string = "01";
        const date_str = year + "-" + month + "-" + day;
        this.getPDF$ = this.usertimeService.getPDF(this.dev.id, date_str)
        .subscribe((res: Blob) => {
            // console.log(typeof(res), res);

            this.downloadFile(res, month, year);
        },
        (err) => {  // Error response from backend (after passing through ErrorInterceptor)
            console.error("Erreur de PDF : ", err);
            this.displayError.emit("PDF indisponible (" + err + ").");
        })
    }

    downloadFile(data: any, month: string, year: string) {
        const blob = new Blob([data], { type: 'application/pdf' });
        saveAs(blob, "pointages" + this.dev.lastname.toUpperCase() + 
            this.dev.firstname.toLowerCase() + "_" + year + "-" + month + ".pdf");
    }
}