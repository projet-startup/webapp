﻿import { Component, OnDestroy, OnInit } from '@angular/core';

import { User, Usertime } from '@/_models';
import { UserService, UsertimeService, AuthenticationService } from '@/_services';
import { Subscription } from 'rxjs';

    
@Component({ templateUrl: 'dev-home.component.html' })
export class DevHomeComponent implements OnInit, OnDestroy {
    currentUser: User;
    manager: User = null;
    errorMsg : string = "";
    modifying : number = -1;  // Index of the usertime being modified. If none, -1
    projects : Array<any> = [];
    modifiedUTs : Array<Usertime>;
    oldest_ut_date : Date = new Date();

    // Variables pour le systeme de pagination
    rowsPerPage: number = 10;
    usertimes: Array<any> = [];
    currUsertimes: Array<any> = [];
    currPage: number = 0;
    maxPage: number = 0;

    refreshUT$ : Subscription;
    userData$ : Subscription;
    deleteUT$ : Subscription;
    updateUT$ : Subscription;

    constructor(
        private authenticationService: AuthenticationService,
        private usertimeService: UsertimeService,
        private userService: UserService,
    ) {
        this.currentUser = this.authenticationService.currentUserValue;
        this.projects = [];
        for (const p of this.currentUser.projects) {
            this.projects.push({value: p.id, name: p.name});
        }
        this.modifiedUTs = [];
    }

    ngOnInit() {
        this.showUserTimes(1);  // Afficher la première page de pointage
        
        this.refreshUsertimes();
        
        this.userData$ = this.userService.getUserData(this.currentUser.managerId)
        .subscribe((res) => {
            if (res == null || !res || Array.isArray(res)) {
                return;  // Le restultat a un soucis de format
            }
            this.manager = res;
        });
    }

    ngOnDestroy() {
        if (this.refreshUT$) this.refreshUT$.unsubscribe();
        if (this.userData$) this.userData$.unsubscribe();
        if (this.deleteUT$) this.deleteUT$.unsubscribe();
    }
    
    displayError(msg : string) {
        this.errorMsg = msg;
    }

    refreshUsertimes() {
        this.refreshUT$ = this.usertimeService.getUsertimes(this.currentUser.id, true)
        .subscribe((res) => {
            this.usertimes = res['usertimes'];
            this.maxPage = Math.ceil(this.usertimes.length / this.rowsPerPage);
            this.showUserTimes(0);

            // Recuperer le plus ancien pointage pour limiter les choix pour le telechargement de pdf
            if (this.usertimes.length > 0) {
                this.oldest_ut_date = this.usertimes[this.usertimes.length - 1].date;
            } else {
                this.oldest_ut_date = new Date();
            }
        },
        (err) => {
            this.displayError("Impossible de mettre à jours les pointages (" + err + ")");
        });
    }

    showUserTimes(page: number) {
        if (page >= this.maxPage || page < 0) {
            console.warn("Tried to access unexisting page of the table!");
            return;
        }
        this.currPage = page;
        this.modifying = -1;
        this.currUsertimes = this.usertimes.slice(this.rowsPerPage * this.currPage, this.rowsPerPage * (this.currPage + 1));
        this.modifiedUTs = JSON.parse(JSON.stringify(this.currUsertimes));  // Deep copy (le js c'est nul)
    }

    
    // EVENTS //
    onPageChange(i: number) {
        this.showUserTimes(i);
    }

    onPageNext() {
        this.showUserTimes(this.currPage + 1);
    }

    onPagePrevious() {
        this.showUserTimes(this.currPage - 1);
    }

    onChangeUsertime(i: number) {
        this.deleteUT$ = this.usertimeService.updateUsertime(this.modifiedUTs[i].id, this.modifiedUTs[i].project.id, this.modifiedUTs[i].date.toString(), this.modifiedUTs[i].time)
        .subscribe((res) => {
            this.modifying = -1;  // Reset table
            this.refreshUsertimes();
        },
        (err) => {
            this.displayError("Modification de pointage a échouée (" + err + ")");
        });
    }

    onDeleteUsertime(i: number) {
        this.deleteUT$ = this.usertimeService.delete(this.currUsertimes[i].id).subscribe(
            () => {
                this.refreshUsertimes();
            },
            (err) => {
                this.displayError("Pointage pas supprimé (" + err + ")");
            }
        );
    }
}