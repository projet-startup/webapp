﻿import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, AuthenticationService } from '@/_services';
import { Subscription } from 'rxjs';

@Component({ templateUrl: 'login.component.html' })
export class LoginComponent implements OnInit, OnDestroy {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    show404Error = false;
    error_msg = "";

    login$ : Subscription;

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }


    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService
    ) {
        // redirect to home if already logged in
        if (this.authenticationService.currentUserValue) {
            this.redirect(this.authenticationService.currentUserValue.role);
        }
    }

    ngOnInit() {        
        this.loginForm = this.formBuilder.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });

        // Si on est redirigé ici parce que le lien n'existait pas
        if (this.route.snapshot.queryParams['not_found_error']) {
            this.show404Error = true;
        }
    }

    ngOnDestroy() {
        if (this.login$) this.login$.unsubscribe();
    }

    redirect(role) {
        // Rediriger vers dev ou manager ou admin. Pour l'instant ca renvoi vers dev
        let homepage = "";
        if (role == "DEV") {
            homepage = "dev-home";
        } else if (role == "MANAGER") {
            homepage = "manager-home";
        } else if (role == "ADMIN") {
            homepage = "admin-home";
        } else {
            homepage = "";
            console.log("/!\\ Warning: account role unknown:", role);
        }
        this.router.navigate([homepage]);
    }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        this.login$ = this.authenticationService.login(this.f.email.value, this.f.password.value)
            .pipe(first())
            .subscribe(
                () => {
                    let userdata : any = this.authenticationService.currentUserValue;
                    console.log("userdata: ", userdata);
                    this.redirect(userdata.role);
                },
                error => {
                    this.error_msg = "Erreur (" + error + ").";
                    this.loading = false;
                });
    }
}
