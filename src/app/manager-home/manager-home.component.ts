﻿import { Component, OnInit } from '@angular/core';

import { DevsProjectsInfos, User, UsertimeSelectedUser } from '@/_models';
import { UserService, AuthenticationService, UsertimeService } from '@/_services';
import { Router } from '@angular/router';

@Component({ templateUrl: 'manager-home.component.html' })
export class ManagerHomeComponent implements OnInit {
    currentUser: User;
    devsProjectsInfos: Array<DevsProjectsInfos> = [];
    errorMsg : string = "";

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private userService: UserService,
        private usertimeService: UsertimeService,
    ) {
        this.currentUser = this.authenticationService.currentUserValue;
    }

    ngOnInit() {
        this.userService.getAll()
            .subscribe((res) => {
                res['users'].forEach(currentValue => {
                    if(currentValue.managerId == this.currentUser.id){
                        let currentDev = new DevsProjectsInfos(currentValue.id, currentValue.firstname, currentValue.lastname, currentValue.email, this.getUsertimeSelectedDeveloper(currentValue.id));
                        this.devsProjectsInfos.push(currentDev);
                    }
                })
            },
            (err) => {
                this.displayError("Impossible de récupérer la liste des développeurs (" + err + ")");
            });
    }

    getUsertimeSelectedDeveloper(developerId: any): Array<UsertimeSelectedUser> {
        let usertimesSelectedDeveloper: Array<UsertimeSelectedUser> = [];
        this.usertimeService.getUsertimesActiveProjects(developerId, true)
            .subscribe((res) => {
                res['projects'].forEach( currentUT => {
                    let totalTimes = this.addTimes(currentUT['usertimes']);
                    let oldest_ut_date = new Date();
                    oldest_ut_date = currentUT['usertimes'][currentUT['usertimes'].length - 1].date;
                    let currentUsertime = new UsertimeSelectedUser(currentUT.id,currentUT.name, totalTimes, 0 ,oldest_ut_date);
                    usertimesSelectedDeveloper.push(currentUsertime);
                });
                this.usertimeService.getUsertimesCR(developerId, true)
                    .subscribe((CRres) => {
                        CRres['projects'].forEach(element => {
                            usertimesSelectedDeveloper.forEach(res => {
                                if (element.id == res.projectId){
                                    let totalTimesCR = this.addTimes(element['usertimes']);
                                    res.curMonth = totalTimesCR;
                                }
                            })
                        });
                    },
                    (err) => {
                        this.displayError("Impossible de récupérer les pointages du mois courant (" + err + ")");
                    });
            },
            (err) => {
                this.displayError("Impossible de récupérer l'ensemble des pointages (" + err + ")");
            });
        return usertimesSelectedDeveloper;
    }

    addTimes(usertimesTab: Array<any>): any {
        let total = 0;
        usertimesTab.forEach((currentUT, index) => {
            total = total + currentUT.time;
        })
        return total;
    }

    deleteDevFromProject(devId: string, projectId: string){
        if (confirm("Voulez-vous vraiment retirer cet utilisateur du projet ?")){
            this.userService.deleteUserFromProject(devId, projectId)
                .subscribe((res) => {
                    location.reload();
                },
                (err) => {
                    this.displayError("Impossible de supprimer l'utilisateur de ce projet (" + err + ")");
                });
        }
        else {
            return;
        }
    }

    displayError(msg : string) {
        this.errorMsg = msg;
    }

}