import { Component, OnInit } from '@angular/core';

import { DevsProjectsInfos, Project, User} from '@/_models';
import { AuthenticationService, ProjectService, UserService } from '@/_services';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({ templateUrl: 'manager-project-view.component.html' })
export class ManagerProjectViewComponent implements OnInit {
    currentUser: User;
    projectList: Array<Project> = [];
    availableDevsList: Array<User> = [];
    errorMsg : string = "";
    devIdSelected: string;
    modifying : number = -1;
    modifiedProjects : Array<Project>;

    deleteProject$: Subscription;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private projectService: ProjectService,
        private userService: UserService
    ) {
        this.currentUser = this.authenticationService.currentUserValue;
        this.modifiedProjects = [];
    }

    ngOnInit(){
        this.projectService.getAll()
            .subscribe((res) => {
                res['projects'].forEach(project => {
                    if(project.isOpen){
                        this.projectService.getDevsFromProject(project.id)
                            .subscribe((res2) => {
                                this.projectList.push(new Project(project.id, project.name, project.description, project.isOpen, res2.devs));
                            },
                            (err) => {
                                this.displayError("Impossible de récupérer la liste des développeurs rattachés aux projets(" + err + ")");
                            });
                    }
                });
            },
            (err) => {
                this.displayError("Impossible de récupérer la liste des projets en cours(" + err + ")");
            });
        this.modifiedProjects = this.projectList;
    }

    getAvailableDevs(projectId: string) {
        this.availableDevsList = [];
        this.userService.getAll()
            .subscribe((res) => {
                res['users'].forEach(currentValue => {
                    if(currentValue['projects'].filter(project => project.id == projectId).length == 0 ){
                        // Cas 1 -------- Utilisateur courant : MANAGER
                        if (this.currentUser.role == "MANAGER"){
                            if (currentValue.managerId == this.currentUser.id){
                                let currentDev = new User(currentValue.id, currentValue.firstname, currentValue.lastname);
                                this.availableDevsList.push(currentDev);
                            }
                        }
                        //Cas 2 --------- Utilisateur courant : ADMIN
                        else if(this.currentUser.role == "ADMIN"){
                            let currentDev = new User(currentValue.id, currentValue.firstname, currentValue.lastname);
                            this.availableDevsList.push(currentDev);
                        }
                    }
                })
            },
            (err) => {
                this.displayError("Impossible de récupérer la liste des développeurs (" + err + ")");
            });
    }

    addDevToProject(projectId: string) {
        this.userService.addUserToProject(this.devIdSelected, projectId)
            .subscribe((res) => {
                location.reload();
            },
            (err) => {
                this.displayError("Impossible d'ajouter ce développeur au projet (" + err + ")");
            });
    }

    displayError(msg : string) {
        this.errorMsg = msg;
    }

    onChangeProject(i: number) {
        console.log("TODO update Project", this.projectList[i], "with", this.modifiedProjects[i]);
        this.deleteProject$ = this.projectService.updateProject(this.modifiedProjects[i].id, this.modifiedProjects[i].name, this.modifiedProjects[i].description)
        .subscribe((res) => {
            console.log(res);
            this.modifying = -1;  // Reset table
            location.reload();
        },
        (err) => {
            this.displayError("Modification de projet a échouée (" + err + ")");
        });
    }

    onDeleteProject(i: number) {
        console.log("DELETE usertime", this.projectList[i]);
        this.deleteProject$ = this.projectService.delete(this.projectList[i].id)
        .subscribe((res) => {
            console.log(res);
            location.reload();
        },
        (err) => {
            this.displayError("Project non supprimé (" + err + ")");
        });
    }
}