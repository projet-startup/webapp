import { Component, OnDestroy, OnInit, Output } from '@angular/core';

import { User } from '@/_models';
import { AuthenticationService, ProjectService } from '@/_services';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';


@Component({selector:"project-create", templateUrl: 'project-create.component.html' })
export class ProjectCreateComponent implements OnInit, OnDestroy {
    currentUser: User;
    createForm: FormGroup;

    projectCreate$: Subscription;

    @Output() displayError : EventEmitter<string> = new EventEmitter<string>();

    // convenience getter for easy access to form fields
    get f() { return this.createForm.controls; }


    constructor(
        private authenticationService: AuthenticationService,
        private formBuilder: FormBuilder,
        private projectService: ProjectService
    ) {
        this.currentUser = this.authenticationService.currentUserValue;
    }

    ngOnInit() {
        this.createForm = this.formBuilder.group({
            projectName: '',
            projectDescription: ''
        });
    }

    ngOnDestroy() {
        if (this.projectCreate$) this.projectCreate$.unsubscribe();
    }

    onSubmit(data) {
        this.projectCreate$ = this.projectService.create(data.projectName, data.projectDescription)
            .subscribe(() => { // Successful response from back end
                location.reload();
            },
            (err) => {  // Error response from backend (after passing through ErrorInterceptor)
                this.displayError.emit("Erreur de création de pointage (" + err + ")");
            });
    }
}